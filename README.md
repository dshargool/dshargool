# Dustin Shargool
## About me
### Professional
Professionally, I am a traditional system administrator that's currently working at [CoolIT](https://www.coolitsystems.com/) in a transition towards DevOps/SRE type roles.  I am currently focusing on wrangling in our development processes so that we can focus on improving the quality and maintainability of our products.

### Personal
I have a few projects on the go at all times, some that I'll never get back to as I'm sure you can appreciate.
#### Current Projects:
1. Digital Ocean Kubernetes Challenge (Need some Sammy slippers!)
    - [Repo](https://gitlab.com/dshargool/do-k8s-log-monitor)
    - [Challenge](https://www.digitalocean.com/community/pages/kubernetes-challenge)
2. Advent of Code
    Made it to day 8 and ran out of time.  Challenges also ramped up so hopefully I make it further next year.  Can also go back and try later.
    - [Repo](https://adventofcode.com/)


## Tech Stack
### Languages
- Python
- Bash
- PowerShell

Want to learn:
- rust
- Go
### DevOps Tools
- Docker
- Ansible
- Terraform
- GitLab CI
- GitHub Actions
